const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const bodyParser = require("body-parser");
const dbConfig = require("./app/config/db-config");

const app = express();
const http = require("http").Server(app);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const io = require("socket.io")(http);

app.use(bodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || 5000;
MongoClient.connect(
  dbConfig.dburl,
  (err, database) => {
    if (err) return console.log(err);
    const db = database.db("heroku_sjp30tgz");
    console.log("db in server.js----" + db.databaseName);
    console.log("socket io server.js----" + io);
    require("./app/routes")(app, db, io);
    var server = http.listen(port, () => {
      console.log("planning poker server port " + server.address().port);
    });
  }
);
