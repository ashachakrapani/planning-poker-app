# planning-poker-app

App Features

The application is built on Node.js, Express, MongoDB. The app is hosted on Heroku and the urls mapped into the project. 
To run the project locally 
1. Clone the project locally using using the details in the repository.
  git clone https://github.com/ashachakrapani/planning-poker-app.git
2. Run npm install to install the dependecies of the project
3. Use 'npm run dev' to start the project
4. The app urls can be tested using Postman. 
