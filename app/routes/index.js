const taskRoutes = require("./task-routes");
const teamRoutes = require("./team-routes");

module.exports = function(app, db, io) {
  taskRoutes(app, db, io);
  teamRoutes(app, db, io);
};
