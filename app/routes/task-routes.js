const _ = require("lodash");
const mongo = require("mongodb");

module.exports = function(app, db, io) {
  // Set socket.io listeners.
  io.on("connection", socket => {
    socket.on("TaskPointsUpdateRequiredEvent", function(
      objectId,
      taskId,
      taskPoint,
      memberId
    ) {
      db.collection("TaskPoints").findAndModify(
        { memberId: memberId, taskId: taskId }, //query
        [], // sort order
        { $set: { taskPoint: taskPoint } }, //upsert
        { upsert: true },
        function(err, result) {
          if (err) {
            //console.log(err);
          } else {
            //console.log(result);
          }
        }
      );
      //console.log("data saved, emitting TaskPointsSummaryEvent... ");

      io.sockets.emit(
        "TaskPointsSummaryEvent",
        objectId,
        taskId,
        taskPoint,
        memberId
      );
    });
  });
  function getNextSequence(db, name, callback) {
    db.collection("TaskIdSequence").findAndModify(
      { _id: name },
      { upsert: true },
      { $inc: { sequence: 1 } },
      function(err, result) {
        if (err) callback(err, result);
        callback(err, result.value.sequence);
      }
    );
  }
  app.get("/", (req, res) => {
    res.json({ message: "planning-poker-app is deployed" });
  });
  app.post("/api/task", (req, res) => {
    getNextSequence(db, "taskId", (err, result) => {
      if (!err) {
        db.collection("Tasks").insert(
          {
            _taskId: result,
            taskTitle: req.body.taskTitle,
            taskDescription: req.body.taskDescription
          },
          (error, insertResult) => {
            if (error) {
              res.status(500).json({ error: "Error creating task." });
            } else {
              res.status(200).json({ taskId: result });
            }
          }
        );
      }
    });
  });
  app.delete("/api/tasks", (req, res) => {
    console.log(req.body.id);
    db.collection("Tasks").deleteOne(
      { _id: new mongo.ObjectID(req.body.id) },
      (error, result) => {
        if (error) {
          res.status(500).json({ error: " Error deleting task" });
        } else {
          res.status(200).json({ objectId: req.body.id });
        }
        res.send(result.n);
      }
    );
  });
  app.get("/api/tasks", (req, res) => {
    let formattedTaskItems = [];
    db.collection("Tasks")
      .find()
      .toArray((err, taskItems) => {
        if (!err) {
          formattedTaskItems = taskItems.map(task => {
            return {
              objectId: task._id,
              taskId: task._taskId,
              taskTitle: task.taskTitle,
              taskDescription: task.taskDescription
            };
          });
          res.status(200).json({ tasks: formattedTaskItems });
        } else {
          res.status(500).json({ error: "An error occured." });
        }
      });
  });
  app.get("/api/tasks/taskdetail", (req, res) => {
    db.collection("TaskPoints")
      .find({ taskId: parseInt(req.query.taskId) })
      .toArray((err, items) => {
        res.send({ items });
      });
  });
};
